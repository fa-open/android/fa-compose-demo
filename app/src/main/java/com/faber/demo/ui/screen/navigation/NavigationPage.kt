package com.faber.demo.ui.screen.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.faber.demo.ui.components.BottomBar
import com.faber.demo.ui.components.SampleScaffold
import com.faber.demo.ui.screen.NavGraphs
import com.faber.demo.ui.screen.destinations.Destination
import com.faber.demo.ui.screen.destinations.HomeScreenDestination
import com.faber.demo.ui.screen.destinations.MineScreenDestination
import com.faber.demo.ui.screen.destinations.NewsScreenDestination
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.animations.rememberAnimatedNavHostEngine

@OptIn(ExperimentalMaterialNavigationApi::class, ExperimentalAnimationApi::class)
@Composable
fun NavigationPage() {
    val engine = rememberAnimatedNavHostEngine()
    val navController = engine.rememberNavController()

    val startRoute = NavGraphs.root.startRoute

    SampleScaffold(
        navController = navController,
        startRoute = startRoute,
        bottomBar = {
            if (it.shouldShowScaffoldElements) {
                BottomBar(navController)
            }
        }
    ) {
        DestinationsNavHost(
            engine = engine,
            navController = navController,
            navGraph = NavGraphs.root,
            modifier = Modifier.padding(it).fillMaxSize(),
            startRoute = startRoute
        )
    }
}



private val Destination.shouldShowScaffoldElements get() = listOf(HomeScreenDestination, NewsScreenDestination, MineScreenDestination).contains(this)
