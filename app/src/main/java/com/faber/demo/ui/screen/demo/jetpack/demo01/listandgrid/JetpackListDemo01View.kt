package com.faber.demo.ui.screen.demo.jetpack.demo01.listandgrid

import androidx.compose.foundation.clickable
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ListItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.ramcosta.composedestinations.annotation.Destination

@Destination
@Composable
fun JetpackListDemo01View() {
    ListSimple()
}

@Preview
@Composable
fun ListSimple() {
    LazyColumn {
        // Add a single item
        item {
            Text(text = "First item")
        }

        // Add 5 items
        items(5) { index ->
            ListItem(
                headlineContent = {
                    Text(text = "Item: $index")
                }
            )
        }

        // Add another single item
        item {
            Text(text = "Last item")
        }
    }
}
