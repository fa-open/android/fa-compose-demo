package com.faber.demo.ui.screen.demo.jetpack.demo01.canvas

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun SimpleBox(
    modifier: Modifier = Modifier,
    width: Dp = 100.dp,
    height: Dp = 100.dp,
    content: @Composable () -> Unit
) {
    Box(
        modifier = modifier
            .width(width)
            .height(height)
            .background(Color.LightGray),
    ) {
        content()
    }
}