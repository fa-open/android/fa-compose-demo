package com.faber.demo.ui.screen.demo.jetpack.demo01.canvas

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.inset
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.faber.demo.ui.components.FaTitleBar
import com.ramcosta.composedestinations.annotation.Destination

@Preview
@Destination
@Composable
fun JetpackCanvasDemo01View() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
    ) {
        FaTitleBar("half rect")
        SimpleBox {
            Canvas(modifier = Modifier.fillMaxSize()) {
                val canvasQuadrantSize = size / 2F
                drawRect(
                    color = Color.Magenta,
                    size = canvasQuadrantSize
                )
            }
        }

        FaTitleBar("draw line")
        SimpleBox {
            Canvas(modifier = Modifier.fillMaxSize()) {
                val canvasWidth = size.width
                val canvasHeight = size.height
                drawLine(
                    start = Offset(x = canvasWidth, y = 0f),
                    end = Offset(x = 0f, y = canvasHeight),
                    color = Color.Blue
                )
            }
        }

        FaTitleBar("Scale")
        SimpleBox {
            Canvas(modifier = Modifier.fillMaxSize()) {
                scale(scaleX = 1f, scaleY = 1.5f) {
                    drawCircle(Color.Blue, radius = 20.dp.toPx())
                }
            }
        }

        FaTitleBar("Translate")
        SimpleBox {
            Canvas(modifier = Modifier.fillMaxSize()) {
                translate(left = 10f, top = -30f) {
                    drawCircle(Color.Blue, radius = 20.dp.toPx())
                }
            }
        }

        FaTitleBar("Rotate")
        SimpleBox {
            Canvas(modifier = Modifier.fillMaxSize()) {
                rotate(degrees = 45F) {
                    drawRect(
                        color = Color.Gray,
                        topLeft = Offset(x = size.width / 3F, y = size.height / 3F),
                        size = size / 3F
                    )
                }
            }
        }

        FaTitleBar("Inset")
        SimpleBox {
            Canvas(modifier = Modifier.fillMaxSize()) {
                val canvasQuadrantSize = size / 2F
                inset(horizontal = 50f, vertical = 30f) {
                    drawRect(color = Color.Green, size = canvasQuadrantSize)
                }
            }
        }

        FaTitleBar("Multiple transformations")
        SimpleBox {
            Canvas(modifier = Modifier.fillMaxSize()) {
                withTransform({
                    translate(left = size.width / 5F)
                    rotate(degrees = 45F)
                }) {
                    drawRect(
                        color = Color.Gray,
                        topLeft = Offset(x = size.width / 3F, y = size.height / 3F),
                        size = size / 3F
                    )
                }
            }
        }
    }
}
