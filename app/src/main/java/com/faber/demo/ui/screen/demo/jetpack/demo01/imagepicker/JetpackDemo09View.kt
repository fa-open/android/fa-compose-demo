package com.faber.demo.ui.screen.demo.jetpack.demo01.imagepicker

import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.faber.demo.config.engine.coil.CoilImageEngine
import com.faber.demo.config.engine.coil.CoilZoomImageEngine
import com.faber.demo.ui.theme.FaComposeDemoTheme
import com.ramcosta.composedestinations.annotation.Destination
import github.leavesczy.matisse.DefaultMediaFilter
import github.leavesczy.matisse.Matisse
import github.leavesczy.matisse.MatisseContract
import github.leavesczy.matisse.MediaResource
import github.leavesczy.matisse.MimeType


@Destination
@Composable
fun JetpackDemo09View() {
    val mediaPickerLauncher =
        rememberLauncherForActivityResult(contract = MatisseContract()) { result: List<MediaResource>? ->
            if (!result.isNullOrEmpty()) {
                val mediaResource = result[0]
                val uri = mediaResource.uri
                val path = mediaResource.path
                val name = mediaResource.name
                val mimeType = mediaResource.mimeType
            }
        }

    fun onClick() {
        Log.d("TAG", "click")
        val matisse = Matisse(
            maxSelectable = 1,
            mediaFilter = DefaultMediaFilter(supportedMimeTypes = MimeType.ofImage()),
            imageEngine = CoilImageEngine(),
            singleMediaType = false,
            captureStrategy = null
        )
        mediaPickerLauncher.launch(matisse)
    }

    fun onClick2() {
        Log.d("TAG", "click")
        val matisse = Matisse(
            maxSelectable = 1,
            mediaFilter = DefaultMediaFilter(supportedMimeTypes = MimeType.ofImage()),
            imageEngine = CoilZoomImageEngine(),
            singleMediaType = false,
            captureStrategy = null
        )
        mediaPickerLauncher.launch(matisse)
    }

    Column(
        modifier = Modifier.fillMaxSize().padding(12.dp)
    ) {
        Button(onClick = { onClick() }) {
            Text("Pick Image")
        }
        Button(onClick = { onClick2() }) {
            Text("Pick Image - Coil Zoom")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun JetpackDemo09ViewPreview() {
    FaComposeDemoTheme {
        JetpackDemo09View()
    }
}


