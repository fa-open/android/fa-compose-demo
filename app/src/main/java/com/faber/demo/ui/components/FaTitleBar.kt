package com.faber.demo.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.faber.demo.ui.screen.demo.jetpack.demo01.bottomBorder

@Composable
fun FaTitleBar(text: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.primaryContainer)
            .bottomBorder(1, MaterialTheme.colorScheme.primary)
    ){
        Text(text = text, modifier = Modifier.padding(10.dp))
    }
}

@Composable
fun FaSubTitleBar(text: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.secondaryContainer)
            .bottomBorder(1, MaterialTheme.colorScheme.primary)
    ){
        Text(text = text, modifier = Modifier.padding(10.dp, 4.dp, 10.dp, 4.dp), fontSize = 10.sp)
    }
}
