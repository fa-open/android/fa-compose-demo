package com.faber.demo.ui.screen.demo.jetpack.demo01.canvas

import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PointMode
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.drawscope.inset
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.drawText
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import com.faber.demo.R
import com.faber.demo.ui.components.FaSubTitleBar
import com.faber.demo.ui.components.FaTitleBar
import com.ramcosta.composedestinations.annotation.Destination

@Preview
@Destination
@Composable
fun JetpackCanvasDemo02View() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
    ) {
//        CanvasTextAndImage()
//        CanvasBasicShapes()

        FaTitleBar("Draw path")
        SimpleBox {
            Spacer(
                modifier = Modifier
                    .drawWithCache {
                        val path = Path()
                        path.moveTo(0f, 0f)
                        path.lineTo(size.width / 2f, size.height / 2f)
                        path.lineTo(size.width, 0f)
                        path.close()
                        onDrawBehind {
                            drawPath(path, Color.Magenta, style = Stroke(width = 10f))
                        }
                    }
                    .fillMaxSize()
            )
        }

        FaTitleBar("Accessing Canvas object")
        SimpleBox {
            val drawable = ShapeDrawable(OvalShape())
            Spacer(
                modifier = Modifier
                    .drawWithContent {
                        drawIntoCanvas { canvas ->
                            drawable.setBounds(0, 0, size.width.toInt(), size.height.toInt())
                            drawable.draw(canvas.nativeCanvas)
                        }
                    }
                    .fillMaxSize()
            )
        }
    }
}

@Composable
fun CanvasTextAndImage() {
    FaTitleBar("Draw text")
    SimpleBox {
        val textMeasurer = rememberTextMeasurer()

        Canvas(modifier = Modifier.fillMaxSize()) {
            drawText(textMeasurer, "Hello")
        }
    }

    FaTitleBar("Measure text")
    SimpleBox {
        val textMeasurer = rememberTextMeasurer()
        val pinkColor = Color(0xFFF48FB1)
        val longTextSample = "Lorem ipsum dolor sit amet"

        Spacer(
            modifier = Modifier
                .drawWithCache {
                    val measuredText =
                        textMeasurer.measure(
                            AnnotatedString(longTextSample),
                            constraints = Constraints.fixedWidth((size.width * 2f / 3f).toInt()),
                            style = TextStyle(fontSize = 18.sp)
                        )

                    onDrawBehind {
                        drawRect(pinkColor, size = measuredText.size.toSize())
                        drawText(measuredText)
                    }
                }
                .fillMaxSize()
        )
    }

    FaTitleBar("Measure text TextOverflow")
    SimpleBox {
        val textMeasurer = rememberTextMeasurer()
        val pinkColor = Color(0xFFF48FB1)
        val longTextSample = "Lorem ipsum dolor sit amet"

        Spacer(
            modifier = Modifier
                .drawWithCache {
                    val measuredText =
                        textMeasurer.measure(
                            AnnotatedString(longTextSample),
                            constraints = Constraints.fixed(
                                width = (size.width / 2f).toInt(),
                                height = (size.height / 2f).toInt()
                            ),
                            overflow = TextOverflow.Ellipsis,
                            style = TextStyle(fontSize = 18.sp)
                        )

                    onDrawBehind {
                        drawRect(pinkColor, size = measuredText.size.toSize())
                        drawText(measuredText)
                    }
                }
                .fillMaxSize()
        )
    }

    FaTitleBar("Draw image")
    SimpleBox {
        val dogImage = ImageBitmap.imageResource(id = R.drawable.dog)

        Canvas(modifier = Modifier.fillMaxSize(), onDraw = {
            scale(scaleX = 0.2f, scaleY = 0.2f) {
                drawImage(dogImage)
            }
        })
    }
}

@Composable
fun CanvasBasicShapes() {
    val purpleColor = Color(0xFFBA68C8)
    val modifier = Modifier
        .fillMaxSize()
        .padding(16.dp)

    FaTitleBar("Draw basic shapes")
    FaSubTitleBar("drawCircle()")
    SimpleBox {
        Canvas(modifier = modifier) {
            drawCircle(purpleColor)
        }
    }

    FaSubTitleBar("drawRect()")
    SimpleBox {
        Canvas(modifier = modifier) {
            drawRect(purpleColor)
        }
    }

    FaSubTitleBar("drawRoundedRect()")
    SimpleBox {
        Canvas(modifier = modifier) {
            drawRoundRect(color = purpleColor, cornerRadius = CornerRadius(20f))
        }
    }

    FaSubTitleBar("drawLine()")
    SimpleBox {
        Canvas(modifier = modifier) {
            drawLine(color = purpleColor, start = Offset.Zero, end = Offset(x = size.width, y = size.height))
        }
    }

    FaSubTitleBar("drawOval()")
    SimpleBox(width = 200.dp) {
        Canvas(modifier = modifier) {
            drawOval(purpleColor)
        }
    }

    FaSubTitleBar("drawArc()")
    SimpleBox {
        Canvas(modifier = modifier) {
            drawArc(
                color = purpleColor,
                startAngle = 0f,
                sweepAngle = 270f,
                useCenter = true,
            )
        }
    }

    FaSubTitleBar("drawPoints()")
    SimpleBox {
        Canvas(modifier = modifier) {
            val half = size.width / 2
            drawPoints(
                points = listOf(
                    Offset(10f, 10f),
                    Offset(half, 10f),
                    Offset(half, half),
                ),
                pointMode = PointMode.Points,
                color = purpleColor,
                strokeWidth = 20f,
            )
        }
    }
}
