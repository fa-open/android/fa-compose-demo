package com.faber.demo.ui.screen.mine

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.ramcosta.composedestinations.annotation.Destination

@Destination
@Composable
fun MineScreen() {
    Text(text = "Mine")
}